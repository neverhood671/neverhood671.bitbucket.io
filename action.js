"use strict"

var emojiSet = ['🐶', '🐰', '🐱', '🐹', '🐭', '🐻'];
var WIN_RESULT = "Win";
var LOSE_RESULT = "Lose";
var MAX_TIME = 59;
var time = MAX_TIME;


function isAllCardsAlreadyPaired() {
    var pairedCard = document.getElementsByClassName("paired");
    var allCards = document.getElementsByClassName("back");
    return pairedCard.length == allCards.length ? true : false;
}

function runTimmer() {
    document.getElementById("minutes").classList.add("running_timmer");
    var minutes = document.getElementById("minutes");
    var interval_id = setInterval(function (minutes) {
        time--;
        document.getElementById("minutes").innerHTML = time > 9 ? time : "0" + time;
        if (isAllCardsAlreadyPaired()) {
            clearInterval(interval_id);
            showModalWindow(WIN_RESULT);
        } else if (time == 0) {
            clearInterval(interval_id);
            showModalWindow(LOSE_RESULT);
        }
    }, 1000);
}

function openCard() {
    if (document.getElementsByClassName("running_timmer").length == 0) {
        runTimmer();
    }
    closeUnpairedCards();
    var otherCardSide = this.nextElementSibling;
    this.classList.remove("closed");
    otherCardSide.classList.remove("closed");
    otherCardSide.classList.add("opened");
    this.classList.add("opened");
    tryToPairCards();
}

function closeCard() {
    var otherCardSide = this.previousElementSibling;
    this.classList.remove("opened");
    otherCardSide.classList.remove("opened");
    otherCardSide.classList.add("closed");
    this.classList.add("closed");
}

function addOnClicksHendlersToCards(className) {
    var cards = document.getElementsByClassName(className);
    var handler = className == "front" ? openCard : closeCard;
    Array.from(cards).forEach(card => {
        card.onclick = function () { handler.call(card) };
    });
}

function shuffleCards() {
    var cards = document.getElementsByClassName("card");
    Array.from(cards).forEach(card => {
        card.classList.remove("opened");
        card.classList.remove("unpaired");
        card.classList.remove("paired");
    });
    addClassToCards();
    fillCardsByEmoji();
}

function fillCardsByEmoji() {
    var cards = document.getElementsByClassName("back");
    var shuffledCardIndexes = shuffle([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
    emojiSet.forEach(function (emoji) {
        cards[shuffledCardIndexes.pop()].innerHTML = emoji;
        cards[shuffledCardIndexes.pop()].innerHTML = emoji;
    });
}

function shuffle(array) {
    var i = array.length,
        j = 0,
        temp;
    while (i--) {
        j = Math.floor(Math.random() * (i + 1));
        // swap randomly chosen element with current element
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function closeUnpairedCards() {
    var unpairedCards = document.getElementsByClassName("unpaired");
    if (unpairedCards.length == 2) {
        unpairedCards[0].classList.add("simple");
        closeCard.call(unpairedCards[0]);
        unpairedCards[1].classList.add("simple");
        closeCard.call(unpairedCards[1]);
        unpairedCards[1].classList.remove("unpaired");
        unpairedCards[0].classList.remove("unpaired");
    }
}

function tryToPairCards() {
    var cards = document.getElementsByClassName("simple opened");
    if (cards.length == 2) {
        if (cards[1].innerHTML == cards[0].innerHTML) {
            cards[0].classList.add("paired");
            cards[1].classList.add("paired");
        } else {
            cards[0].classList.add("unpaired");
            cards[1].classList.add("unpaired");
        }
        cards[1].classList.remove("simple");
        cards[0].classList.remove("simple");
    }
}

function addClassToCards() {
    var cards = document.getElementsByClassName("back");
    Array.from(cards).forEach(card => {
        card.classList.add("simple");
    });
}

function addHandlerOnButton() {
    var button = document.getElementsByClassName("modal_window_button")[0];
    button.onclick = function () {
        document.getElementsByClassName("result_window")[0].classList.remove("game_over");
        document.getElementsByClassName("block_layer")[0].classList.remove("game_over");
        time = MAX_TIME;
        document.getElementById("minutes").innerHTML = time;
        shuffleCards();
        document.getElementsByClassName("running_timmer")[0].classList.remove("running_timmer");
    }
}

function showModalWindow(result) {
    document.getElementsByClassName("result_window")[0].classList.add("game_over");
    document.getElementsByClassName("block_layer")[0].classList.add("game_over");
    var resultSpans = "";
    for (var i = 0; i < result.length; i++) {
        resultSpans += "<span class='letter'>" + result[i] + "</span>";
    }
    document.getElementsByClassName("game_result")[0].innerHTML = resultSpans;
    addAnimationOnResultWindow();
}

function addAnimationOnResultWindow() {
    var letters = document.getElementsByClassName("letter");
    Array.from(letters).forEach(function (letter, i) {
        var style = "letter_scaling 0.7s " + (i * 0.1) + "s infinite";
        letter.style.animation = style;
    });
}


addClassToCards();
addOnClicksHendlersToCards("front");
addOnClicksHendlersToCards("back");
fillCardsByEmoji();
addHandlerOnButton();
